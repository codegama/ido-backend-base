<div class="row">

    <div class="col-lg-12 col-12">

        <div class="box">
            <div class="box-header with-border">
                
                <h3 class="box-title">{{ $subscription->id ? tr('edit_subscription') : tr('add_subscription') }}</h3>

            </div>
                
            <form action="{{ (Setting::get('is_demo_control_enabled') == YES) ? '#' : route('admin.subscriptions.save') }}" method="POST" enctype="multipart/form-data" role="form">
                
                @csrf

                <div class="box-body">

                    <div class="row">

                        <input type="hidden" name="subscription_id" id="subscription_id" value="{{ $subscription->id}}">

                        <div class="col-md-6">
                                
                            <label for="title">{{ tr('title') }}*</label>

                            <div class="form-group">                                  
                                
                                <input type="text" id="title" name="title" class="form-control" placeholder="{{ tr('title') }}" value="{{old('amount') ?: $subscription->title}}" required>

                            </div>

                        </div>

                        <div class="col-md-6">
                                
                            <label for="amount">{{tr('amount')}}*</label>

                            <div class="form-group">                                  
                                
                                <input type="number" id="amount" name="amount" class="form-control" placeholder="{{tr('amount')}}" value="{{old('amount') ?: $subscription->amount}}" required>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <label for="no_of_projects">{{ tr('no_of_projects') }} <span class="admin-required">*</span></label>

                            <div class="form-group">

                                <input type="number" min="1" step="any" required name="no_of_projects" class="form-control" id="no_of_projects" value="{{ old('no_of_projects') ?: $subscription->no_of_projects }}" title="{{ tr('no_of_projects') }}" placeholder="{{ tr('no_of_projects') }}">
                            </div>
                        
                        </div>

                        <div class="col-md-6">

                            <label for="plan">{{ tr('plan_validity') }} <span class="admin-required">*</span></label>

                            <div class="form-group">

                                <input type="number" min="1" required name="plan" class="form-control" id="plan" value="{{ old('plan') ?: $subscription->plan }}" title="{{ tr('plan') }}" placeholder="{{ tr('plan') }}">
                            </div>
                        
                        </div>

                        <div class="col-md-6">

                            <label for="plan_type">{{ tr('plan_type') }} <span class="admin-required">*</span></label>

                            <div class="form-group">

                                <select class="form-control select2" id="plan_type" name="plan_type" required="">
                                    <option value="">{{tr('select_plan_type')}}</option>

                                    @foreach($subscription_plan_types as $subscription_plan_type)
                                    <option value="{{$subscription_plan_type}}" @if($subscription->plan_type == $subscription_plan_type) selected @endif>
                                        {{ucfirst($subscription_plan_type)}}
                                    </option>
                                    @endforeach

                                </select>
                            </div>
                        
                        </div>

                        <div class="col-md-6" style="display: none;">

                            <label for="plan">{{ tr('is_popular') }} <span class="admin-required">*</span></label>

                            <div class="form-group">

                                <!-- <input type="checkbox" name="is_popular" @if($subscription->is_popular == YES) checked @endif > -->
                            </div>
                        
                        </div>

                        <div class="col-md-12">

                            <label for="simpleMde">{{ tr('description') }}</label>

                            <div class="form-group">

                                <textarea class="form-control" rows="3" placeholder="Enter ..." id="description" name="description">{{ old('description') ?: $subscription->description}}</textarea>
                            </div>
                        </div>


                    </div>

                </div>

                <div class="box-footer">
                    
                    <button type="reset" class="btn btn-warning btn-default btn-squared px-30">{{tr('reset')}}</button>

                    <button type="submit" class="btn btn-info pull-right">{{tr('submit')}}</button>

                </div>

            </form>

        </div>

    </div>

</div>